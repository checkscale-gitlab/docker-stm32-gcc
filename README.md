# stm32-gcc

This repository sets up a simple Docker image, based on Ubuntu 18.04 and a locally provided tarball of the STM32 toolchain. The toolchain tarball comes from the installer at http://www.openstm32.org


## Usage

This image is intended to be used for STM32 automated builds. An appropriate GitLab CI configuration will be needed for the relevant project, for example something like:

```
image: extelopensource/stm32-gcc:gcc-7-2017-q4-major_gdb-5_4-2016q3

variables:
  GIT_SUBMODULE_STRATEGY: recursive

build:
  stage: build
  script:
  - cd project
  - ./build_all.sh
  artifacts:
    name: "project_$CI_COMMIT_REF_NAME"
    paths:
    - project/release/production.bin
```

This assumes that there is an existing shell script which actually performs the build process. 


## Building this image

The required version of the STM32 toolchain needs to be added to this repository. This is expected to have a format like `${ST_GCC_VER}-linux.tar.bz2`.

The actual version name of the toolchain is then passed into Docker as an argument, like so:

```
export ST_GCC_VER=st-gnu-arm-gcc-7-2017-q4-major_gdb-5_4-2016q3
docker build --pull --build-arg ST_GCC_VER=$ST_GCC_VER -t extelopensource/stm32-gcc:$ST_GCC_VER .
```

This will create a Docker image tagged with the toolchain version. 


## Uploading this image

The built image can then be pushed up to Docker Hub, provided access to the "extelopensource" account is available. 

First, login with:

```
$ sudo docker login -u extelopensource -p <PASSWORD>
```

_(for some reason `sudo` seems to be necessary, try without first but it might fail)_

Then push the image with:

```
$ sudo docker push extelopensource/stm32-gcc:$ST_GCC_VER
```

